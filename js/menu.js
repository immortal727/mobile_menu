/*
Необходимо сверстать меню и написать файл menu.js,
который при загрузке страницы запрашивает мобильное меню с сервера и строит его.
Необходимо сверстать меню и написать файл menu.js,
который при загрузке страницы запрашивает мобильное меню с сервера и строит его.
Пункты меню кликабельны (это ссылки), но те, у которых есть дети,
должны также раскрываться по какому-нибудь принципу (в примере выше - стрелки рядом с пункатами).
Внешний вид может быть любым, но обязательно наличие хотя бы какой-нибудь анимации при переходе между экранами.
*/

$(function () {
    // JSON-объект с данными меню
    let menuData = [
        {
            title: 'Главная',
            link: '#'
        },
        {
            title: 'О нас',
            link: '#',
            children: [
                {
                    title: 'Команда',
                    link: '#'
                },
                {
                    title: 'История',
                    link: '#'
                }
            ]
        },
        {
            title: 'Услуги',
            link: '#',
            children: [
                {
                    title: 'Разработка',
                    link: '#'
                },
                {
                    title: 'Дизайн',
                    link: '#'
                },
                {
                    title: 'Маркетинг',
                    link: '#',
                    children: [
                        {
                            title: 'Конверсионный',
                            link: '#'
                        },
                        {
                            title: 'Стимулирующий',
                            link: '#'
                        },
                        {
                            title: 'Развивающий',
                            link: '#'
                        },
                    ]
                }
            ]
        },
        {
            title: 'Контакты',
            link: '#'
        }
    ]

    let prevItems = []; // Массив для сохранения предыдущих элементов

    function buildMenu(data, parentClass) {
        let ul = $('<ul>').addClass(parentClass);

        $.each(data, function(index, item) {
            let li = $('<li>');
            let link = $('<a>').attr('href', item.link).text(item.title);

            if (item.children) {
                li.addClass('dropdown');

                let arrow = $('<span>').addClass('arrow arrow-right');
                arrow.on('click', function(e) {
                    e.stopPropagation();

                    // Скрывать остальные верхние уровни меню
                    $(this).find("ul").slideUp();

                    if (li.hasClass('active')) {
                        li.removeClass('active');
                        li.find('ul').slideUp();
                    } else {
                        // Показывать текущий уровень меню
                        li.addClass('active');
                        li.find('ul').slideDown();

                        // Скрыть первый элемент <a> и ссылку span.arrow
                        li.find('a.parent_link:first').hide();
                        li.find('a.parent_link:first').next('span.arrow').hide();

                        // Скрывать остальные пункты меню
                        li.siblings().hide();
                        li.siblings().find('ul').hide();
                    }

                    // Показывать заголовок как ссылку перед основным <ul>
                    $('.show-heading').remove();

                    let back = '<input type="button" class="btn_back" value="Назад"/>'
                    if(arrow.closest('ul.submenu') && $('#topnav').find('.btn_back').length===0) {
                        $("ul.show").first().before(back);
                    }
                    let headingLink = $('<a>').addClass('show-heading').attr('href', item.link).text(item.title);
                    $("li.active:first").prepend(headingLink);
                });

                li.append(link);
                li.append(arrow);
                link.addClass('parent_link');

                let submenu = buildMenu(item.children, 'submenu');
                // Скрыть все дочерние элементы по умолчанию
                submenu.hide();

                li.append(submenu);
            } else {
                li.append(link);
            }

            ul.append(li);
        });
        return ul;
    }

    $('#topnav').append(buildMenu(menuData, 'show'));
})

$(document).on('click', '.btn_back', function() {
    // Найти текущий активный пункт меню
    let activeItem = $("li.active");
    let children = activeItem.children('ul');
    console.log(children.length)
    // Если текущий активный пункт меню имеет родительский пункт меню
    if (children.length >= 2) {
        activeItem.find('li').show()
        //activeItem.find('.submenu').slideUp()
        // Удалить заголовок текущего активного пункта меню из DOM
        $('.show-heading').remove()

        // Находим родительские пункты
        let parentItem = activeItem.parent()
        parentItem.find('a.parent_link').show()
        parentItem.find('span.arrow').show()

    }
    else {
        $('.show > li').show()
        // Скрыть подменю текущего активного пункта меню и показать все остальные пункты меню
        activeItem.find('ul.submenu').slideUp()

        // Удалить заголовок текущего активного пункта меню из DOM
        $('.show-heading').remove()

        // Находим родительские пункты
        let parentItem = activeItem.parent()
        parentItem.find('a.parent_link').show()
        // Показываем span c классом arrow
        parentItem.find('span.arrow').show()
        $(this).remove()
    }

    // Удалить класс active с текущего активного пункта меню
    activeItem.removeClass('active');
});
